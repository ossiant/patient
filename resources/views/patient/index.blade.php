@extends('patient.layout.main')
@section('content')
    <div class='container'>

        @if(isset ($message))
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  </div>
        @endif



        <a href="{{route('addpatient')}}" class="btn btn-primary">addpatient</a>
            <hr>
        <table class="table table-respnosive table-bordered">

            <thead>
            <tr>
            <th>id</th>
            <th>name</th>
            <th>address</th>
            <th>gender</th>
            <th>disease</th>
                <th>action</th>


            </tr>

        </thead>
        <tbody>

        @if(isset($patients))

            @foreach($patients as $patient_info)
                {{--{{ dd ($patient) }}--}}
                <tr>
                    <td>{{$patient_info['id']}}</td>
                    <td>{{$patient_info['name']}}</td>
                    <td>{{$patient_info['address']}}</td>
                    <td>{{$patient_info['gender']}}</td>
                    <td>{{$patient_info['disease']}}</td>
                    <td>
                        <a href="{{route('editpatient',['id'=>$patient_info['id']])}}" class="btn btn-primary">Edit</a>
                    <a href="{{route('deletepatient',['id'=>$patient_info['id']])}}" class="btn btn-primary">Delete</a>
                    </td>
                </tr>


            @endforeach
            @endif

        </tbody>
    </table>
    </div>


    @endsection