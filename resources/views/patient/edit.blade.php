@extends('patient.layout.main')
@section('content')
    @if($patient)

    <div class='container'>
        <form action="{{route('updatepatient')}}" method="POST">
            <div class="form-group">

                    <div class="form-group">
                        <label for="Patient Name"> Patient Name</label>
                        <input type="text"  id="name" name="name" value="{{$patient['name']}}" class="form-group">
                    </div>
                <label for="Patient address"> Patient Address</label>
                <input type="text"  id="address" name="address" value="{{$patient['address']}}" class="form-group">
            </div>
            <div class="form-group">
                <label for="Patient gender"> Patient gender</label>
                <input type="text"  id="gender" name="gender" value="{{$patient['gender']}}" class="form-group">
            </div>



            <div class="form-group">
                <label for="disease">Disease</label>
                <input type="text"  id="gender" name="disease"  value="{{$patient['disease']}}"class="form-group">
            </div>


            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="submit"  name="update" value="update" class="btn btn-primary">
            <input type="hidden" name="id" value="{{$patient['id']}}">
        </form>


    </div>
    @endif;



@endsection