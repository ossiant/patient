<?php

namespace App\Http\Controllers;

use App\PatientInfo;
use Illuminate\Http\Request;
use DB;

class PatientController extends Controller
{
    //
    public function showHome()
    {
        try {
            $result = PatientInfo::all()->toArray();

           // print_r(count($result));
             //die();
            if (count($result) == 0) {

                return view('patient.index')->with(['message' => 'Database is empty']);
            } else {
               // die('sdafsda');
                return view('patient.index')->with(['patients' => $result]);
            }




        } catch (\Exception $e) {
            return $e;
        }

    }

    public function getSinglePatient($id)
    {
        try {
            $result = PatientInfo::find($id)->first()->toArray();
           // print_r($result);
            if (count($result) == 0) {

                return view('patient.edit')->with(['message' => 'Database is empty']);
            } else {
                return view('patient.edit')->with(['patient' => $result]);
            }

        } catch (\Exception $e) {
            return $e;
        }
    }


    public function showPatientForm()
    {
        return view('patient.create');
    }

    public function insertPatient(Request $request)
    {
        try {
            $patientobj = new PatientInfo();

            $patientobj->name = $request->input('name');
            $patientobj->address = $request->input('address');
            $patientobj->gender = $request->input('gender');
            $patientobj->disease = $request->input('disease');


            $patientobj->save();
            //print-r($patientobj);die();

            if ($patientobj->save()) {
                return redirect()->route('listpatient')->with(['message' => 'Inserted successfully']);
            }
        } catch (\Exception $e) {
            return $e;

        }
    }

    public function updatePatient(Request $request)
    {
        try {
            $id = $request->get('id');


            $patientobj = PatientInfo::find($id);

            $patientobj->name = $request->input('name');
            $patientobj->address = $request->input('address');
            $patientobj->gender = $request->input('gender');
            $patientobj->disease = $request->input('disease');


            $patientobj->save();
            //print-r($patientobj);die();

            if ($patientobj->save()) {
                return redirect()->route('listpatient')->with(['message' => 'updeted successfully']);
            }
        } catch (\Exception $e) {
            return $e;

        }
    }


    public function deletePatient($id)
    {
        try {
            DB::table('patient_info')->where('id', '=', $id)->delete();
            return redirect()->route('listpatient')->with(['message' => 'deleted successfully']);

        } catch (\Exception $e) {
            return $e;

        }

    }
}

