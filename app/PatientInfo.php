<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientInfo extends Model
{
    //
    protected $table='patient_info';
}
