<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (){
 return view('welcome');
});
Route::get('/patient',['uses'=>'PatientController@showHome','as'=>'listpatient']);
Route::get('/patient/create',['uses'=>'PatientController@showPatientForm','as'=>'addpatient']);
Route::get('/patient/edit/{id}',['uses'=>'PatientController@getSinglePatient','as'=>'editpatient']);

Route::post('/patient/create',['uses'=>'PatientController@insertPatient','as'=>'insertpatient']);
Route::post('/patient/update',['uses'=>'PatientController@updatePatient','as'=>'updatepatient']);
Route::get('/patient/delete/{id}',['uses'=>'PatientController@deletePatient','as'=>'deletepatient']);